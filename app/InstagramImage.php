<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstagramImage extends Model
{
    //
	protected $fillable = ['instagram_canonical_url'];

	public function location() {
			return $this->belongsTo('App\FoursquareLocation');
	}
}
