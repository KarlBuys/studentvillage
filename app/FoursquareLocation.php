<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoursquareLocation extends Model
{
    //
	protected $fillable = ['foursquare_id', 'foursquare_name'];

	public function instagramImage() {
		return $this->hasMany('App\InstagramImage');
	}

	public function users() {
		return $this->belongsTo('App\Models\User', 'id');
	}

}
