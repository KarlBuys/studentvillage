<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InstagramImage;
use App\FoursquareLocation;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class SVInstagramController extends Controller
{
    //
    protected $access_token = '4564057869.95caca3.be771e3d6146497f8c767ef6976fa75c';

    private function getInstagramImagesByLL(Request $request, $lat, $long) {
      $client = new Client();
      $ll = $lat . ',' . $long;
      $res = $client->request('GET', 'https://api.instagram.com/v1/media/search', [
        'query' => ['lat' => $lat, 'lng' => $long, 'distance' => '5000', 'access_token' => $this->access_token]
      ]);
      $response = json_decode($res->getBody());
      foreach ($response->data as $key => $var) {
        $image = InstagramImage::firstOrNew(['instagram_canonical_url' => $var->link]);
        $image->low_res_url = $var->images->low_resolution->url;
        $image->tn_res_url = $var->images->thumbnail->url;
        $image->standard_res_url = $var->images->standard_resolution->url;
        $image->lat = $var->location->latitude;
        $image->long = $var->location->longitude;
        $image->user_full_name = $var->user->full_name;
        $image->user_profile_pic_url = $var->user->profile_picture;
        //$image->instagram_canonical_url = $data->link;
        $image->save();
      }
      $images = InstagramImage::all();
      return $images->toJson();
    }

    public static function populateImagesForVenue($venueId) {
      echo $venueId;
      $venue = FoursquareLocation::find($venueId);
      //print_r($venue);
      $client = new Client();
      //$ll = $lat . ',' . $long;
      $res = $client->request('GET', 'https://api.instagram.com/v1/media/search', [
        'query' => ['lat' => $venue->venueLat, 'lng' => $venue->venueLong, 'distance' => '5000', 'access_token' => '4564057869.95caca3.be771e3d6146497f8c767ef6976fa75c']
      ]);
      $response = json_decode($res->getBody());
      foreach ($response->data as $key => $var) {
        $image = InstagramImage::firstOrNew(['instagram_canonical_url' => $var->link, 'foursquare_location_id' => $venueId]);
        $image->low_res_url = $var->images->low_resolution->url;
        $image->tn_res_url = $var->images->thumbnail->url;
        $image->standard_res_url = $var->images->standard_resolution->url;
        $image->lat = $var->location->latitude;
        $image->long = $var->location->longitude;
        $image->user_full_name = $var->user->full_name;
        $image->user_profile_pic_url = $var->user->profile_picture;
        //$image->instagram_canonical_url = $data->link;
        $venue->instagramImage()->save($image);
        //$image->save();

      }
    }

    public function getInstagramImagesByVenueId(Request $request, $venueId, $offset) {
      $images = InstagramImage::where('foursquare_location_id', $venueId);
      $count = InstagramImage::where('foursquare_location_id', $venueId)->count();
      $skip = $offset * 10;
      $collection = InstagramImage::where('foursquare_location_id', $venueId)->skip($skip)->take(10)->get();
      $returnArr['data'] = $collection->toArray();
      $returnArr['count'] = $count;
      return json_encode($returnArr);

    }
}
