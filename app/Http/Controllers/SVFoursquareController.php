<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FoursquareLocation;
use App\InstagramImage;
use App\Http\Controllers\SVInstagramController;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
class SVFoursquareController extends Controller
{
    //
    protected $instagram_access_token = '4564057869.95caca3.be771e3d6146497f8c767ef6976fa75c';

    public function addFoursquareVenue(Request $request, $id) {
      $client = new Client();
      $url = 'https://api.foursquare.com/v2/venues/' . $id;
      $res = $client->request('GET', $url, [
        'query' => ['client_id' => '5OAZ2GY21LZDL534GZWWRNQPT2ELXQWVHL1QYYNBRSNUX52S', 'client_secret' => 'I5XU5MEMLVVDV0NMGCG3MY50PAXMTW0N2AQPSTXR0MFO0GKE', 'v' => date('Ymd')]
      ]);

      $response = json_decode($res->getBody());
      $venue = FoursquareLocation::firstOrNew(['venueId' => $id]);
      $venue->venueId = $id;
      $venue->venueName = $response->response->venue->name;
      $venue->venuePhone = isset($response->response->venue->contact->formattedPhone) ? $response->response->venue->contact->formattedPhone : '';
      $venue->venueAddress = $response->response->venue->location->address;
      $venue->venueLat = $response->response->venue->location->lat;
      $venue->venueLong = $response->response->venue->location->lng;
      $venue->save();
      SVInstagramController::populateImagesForVenue($venue->id);
      //return $res->getBody();
    }

    public function getFoursquareLocations() {
      $locations = FoursquareLocation::all();
      return $locations->toJson();
    }

    public function searchFoursquareByLL(Request $request, $lat, $long) {
      $ll = $lat . ',' . $long;
      $res = $client->request('GET', 'https://api.foursquare.com/v2/venues/search', [
        'query' => ['ll' => $ll, 'client_id' => '5OAZ2GY21LZDL534GZWWRNQPT2ELXQWVHL1QYYNBRSNUX52S', 'client_secret' => 'I5XU5MEMLVVDV0NMGCG3MY50PAXMTW0N2AQPSTXR0MFO0GKE', 'v' => date('Ymd')]
      ]);
      print_r($response);
    }
}
