<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
class SVController extends Controller
{
  /**
   * Homepage
   *
   * @return \Illuminate\Http\Response
   */
   public function home()
   {
     if (Auth::check()) {
       return redirect('/loggedin');
     } else {
       return view('welcome');
     }
   }

   public function loggedin()
   {
       return view('loggedin');
   }

    public function privacyPolicy() {
      echo "I plan to do precisely nothing that involves requiring authentication from anyone else who uses this application. I only want to use publically available content. Anyone using this application will not be required to provide access permissions to this app. The application will store their e-mail address and a hashed version of their password for login purposes.";
    }
}
