<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstagramImagesTable extends Migration
{
    /**
     * Run the migrations.
     * @table instagram_images
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram_images', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('low_res_url')->nullable()->default(null);
            $table->text('tn_res_url')->nullable()->default(null);
            $table->text('standard_res_url')->nullable()->default(null);
            $table->string('lat', 30)->nullable()->default(null);
            $table->string('long', 30)->nullable()->default(null);
            $table->string('location_name')->nullable()->default(null);
            $table->string('user_full_name')->nullable()->default(null);
            $table->text('user_profile_pic_url')->nullable()->default(null);
            $table->text('instagram_canonical_url')->nullable()->default(null);
            $table->integer('foursquare_location_id')->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('instagram_images');
     }
}
