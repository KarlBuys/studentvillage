<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoursquareLocationsTable extends Migration
{
    /**
     * Run the migrations.
     * @table foursquare_locations
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foursquare_locations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('venueId')->nullable()->default(null);
            $table->string('venueName')->nullable()->default(null);
            $table->string('venuePhone', 18)->nullable()->default(null);
            $table->text('venueAddress')->nullable()->default(null);
            $table->string('venueLat', 30)->nullable()->default(null);
            $table->string('venueLong', 30)->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('foursquare_locations');
     }
}
