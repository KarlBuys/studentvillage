<ul class="nav nav-sidebar">
    <li>
        <a href="{!! url('loggedin') !!}"><span class="fa fa-dashboard"></span> Dashboard</a>
    </li>
    <li>
        <a href="{!! url('user/settings') !!}"><span class="fa fa-user"></span> Settings</a>
    </li>
    @if (Gate::allows('admin'))
        <li class="sidebar-header"><span>Admin</span></li>
        <li>
            <a href="{!! url('admin/users') !!}"><span class="fa fa-users"></span> Users</a>
        </li>
        <li>
            <a href="{!! url('admin/roles') !!}"><span class="fa fa-lock"></span> Roles</a>
        </li>
    @endif
</ul>
