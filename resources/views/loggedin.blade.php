@extends('layouts.master')

@section('app-content')

    <div class="col-sm-3 col-md-2 sidebar">
        <div class="raw-margin-bottom-90">
            @include('dashboard.panel')
        </div>
    </div>

    <div id="app" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

        @{{ message }}
        <div class="four columns">
          <label for="captureVenueId">Enter a Foursquare venue ID to add it</label>
          <input v-model="venueId" type="text" class = "u-full-width" name='venueId'>
          <button v-on:click="addFoursquareVenue">Add it!</button>
        </div>

        <table id="dataTable">
          <thead>
            <tr>
              <th>Foursquare Venue ID</th>
              <th class='center'>Name</th>
              <th class='center'>Address</th>
              <th class='center'>Phone</th>
              <th class='center'>Latitude</th>
              <th class='center'>Longitude</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="venue in venues">
              <td class='center'>@{{ venue.venueId }}</td>
              <td class='center'>@{{ venue.venueName }}</td>
              <td class='center'>@{{ venue.venueAddress }}</td>
              <td class='center'>@{{ venue.venuePhone }}</td>
              <td class='center'>@{{ venue.venueLat }}</td>
              <td class='center'>@{{ venue.venueLong }}</td>
              <td><button v-on:click="displayImages(venue.id)">View images from this location</button></td>
            </tr>
          </tbody>
        </table>
        <div class='imageDiv'>
          <table>
            <tr>
              <th>Instagram URL</th>
              <th>Thumbnail</th>
              <th>Latitude</th>
              <th>Longitude</th>
              <th>User's name</th>
              <th>User profile picture</th>
            </tr>
            <tr v-for="image in images">
              <td><a v-bind:href="image.instagram_canonical_url">@{{ image.instagram_canonical_url }}</a></td>
              <td><img v-bind:src="image.tn_res_url"></td>
              <td>@{{ image.lat }}</td>
              <td>@{{ image.long }}</td>
              <td>@{{ image.user_full_name }}</td>
              <td><img v-bind:src="image.user_profile_pic_url"></td>
            </tr>
          </table>
        </div>
        <div id="pagination">

        </div>
        <script>
        var BASE_URL = 'http://dev18.karlbuys.com/';
        var currentOffset = 0;

        function displayImages(venueId, offset) {

          var endPoint = 'instagram/getInstagramImagesByVenueId/'+venueId+'/'+offset;
          var thisVenueId = venueId;
          var url = BASE_URL.concat(endPoint);
          $.ajax({
                  url: url,
                  dataType: "json",
                  success: function (data) {
                      console.log(data);
                      vueApp.images = data.data;
                  },
                  error: function (error) {
                      console.log(error);
                  }
              }).done(function(data) {
                // offset, venueId
                console.log(data);
                console.log(offset);
                console.log(currentOffset);
                console.log(thisVenueId);
                createPaginationLinks(data.count, thisVenueId);
              });
        }

        function createPaginationLinks(count, venueId) {
          console.log(count);
          console.log(venueId);
          var pageNum = count / 10;
          console.log(pageNum);
          pageNum = Math.ceil(pageNum);
          console.log(pageNum);
          // we need pageNum number of links
          var linkHtml = '';
          for (i = 0; i < pageNum; i++) {
            linkNum = i+1;
            linkHtml += '<a href="#" class="navLink" onClick="displayImages(' + venueId + ', ' + i + ')">Page ' + linkNum + ' | </a>';
          }
          $('#pagination').html(linkHtml);
        }

        function addFoursquareVenue() {

          var endPoint = 'foursquare/addFoursquareVenue/'+vueApp.venueId;
          var url = BASE_URL.concat(endPoint);
          $.ajax({
                  url: url,
                  dataType: "json",
                  success: function (data) {
                      console.log(data);
                      vueApp.images = data.data;
                  },
                  error: function (error) {
                      console.log(error);
                  }
              }).done(function() {
                initialiseExistingLocations();
                console.log('A new venue!');
              });
        }

        function initialiseExistingLocations() {
          var endPoint = 'foursquare/getLocations';
          var url = BASE_URL.concat(endPoint);
          $.ajax({
                  url: url,
                  dataType: "json",
                  success: function (data) {
                      console.log(data);
                      vueApp.venues = data;
                  },
                  error: function (error) {
                      //console.log(error);
                  }
              });
        }

          var vueApp = new Vue({
            el: '#app',
            data: {
              venues: [
                { venueId: '', venueLat: '', venueLong: '', venueName: '' }
              ],
              images: [
                { id: '', lat: '', long: '' }
              ],
              venueId: '',
              message: ''
            },
            mounted: function () {
              var self = this;
              initialiseExistingLocations();

            }
          });
        </script>
    </div>

@stop
